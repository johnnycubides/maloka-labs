# Firmware para distribuir

Se trata de una copia del firmware lista para instalar en las
tarjetas de desarrollo nodemcu con memoria igual a 4MB;
las placas que cumplen con estas características son las nodemcu V3.

## Lista de firmware

* `catalejo-firmware-4MB.tar.gz`: firmware listo para correr en el nodemcu v3
* `catalejo-firmware-4MB-TEST.tar.gz`: Se trata del firmware listo para correr en el nodemcu v3, pero con una prueba de parpadeo del LED en el
pin 4; esta instalación y test es útil cuando se requiere instalar muchas placas al tiempo y queremos optimizar en el proceso de pruebas de
una correcta instalación.

