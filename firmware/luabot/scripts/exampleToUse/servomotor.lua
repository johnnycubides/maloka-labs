-- example to servomotor
-- perido 20ms -> 50Hz
-- [0°:180°]grados, [33:106] de 1023 ciclo útil
-- Acondicionamiento para que cada entero sea equivalente a un grado
-- 106 - 33 = 73
-- Frec = (20mS*73/180)^-1 = 123 Hz
-- [0°:180°] ← {33, 106}*180/73 = {81, 261}
-- Así entonces 81 corresponde a 0° y 261° corresponde a 180°
-- Author: Johnny Cubides <johnnycubides@catalejoplus.com>

function init_servo(pin,angle)
pwm.setup(pin, 123, 81+angle)
pwm.start(pin)
end

function angle_servo(pin,angle)
pwm.setduty(pin,81+angle)
end

function off_servo(pin)
pwm.stop(pin)
end

init_servo(1,0)

mi_alarma = tmr.create()
mi_alarma:register( 1500, tmr.ALARM_AUTO, function()
if val == true then
val= false
angle_servo(1,0)
else
val= true
angle_servo(1,180)
end
end)
mi_alarma:start()
