tmr.delay(1000000)
gpio.mode(4, gpio.OUTPUT)

function start()
  dofile("wifi.lua")
	gpio.mode(9,gpio.INPUT,gpio.PULLUP)
	gpio.write(4,gpio.LOW)
  print(wifi.ap.getip())
  if gpio.read(9) == 0 then
    uart.setup(0, 115200,8,0,1,1)
    print("config ap-st")
    collectgarbage()
    dofile('apst.lua')
  else
    uart.setup(0, 115200,8,0,1,1)
    print("server.lua")
    collectgarbage()
    dofile('server.lua')
  end
end

start()
