#!/bin/bash -e
#
# Brief:	Instalador de dependencias para el proyecto
# Author: Johnny Cubides
# e-mail:  <jgcubidesc@gmail.com> <johnnycubides@catalejoplus.com>
# date: Monday 29 April 2019

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

OURS_BLOCKLY=../../../catalejo-editor/blockly
# se requiere cambial el contenido de la variable por éste ↓↓↓
# OURS_BLOCKLY=../../../catalejo-editor/oursBlockly

if [[ ! -e $OURS_BLOCKLY ]]; then
  # No se ubica esa dependencia por lo tanto debe ser construida.
  printf "${YELLOW}Creando dependencia:${NC} $OURS_BLOCKLY\n"
  cd ../../../
  git clone -b oursBlockly --single-branch https://gitlab.com/catalejo_team/catalejo-dev/catalejo-editor.git
  printf " ${GREEN}Dependencia creada:${NC} $OURS_BLOCKLY\n"
else
  printf "${BLUE}Ya existe la dependencia:${NC} $OURS_BLOCKLY\n"
fi

HTML=../../../html/catalejo-editor-mobile

if [[ ! -e $HTML ]]; then
  # No se ubica esa dependencia por lo tanto debe ser construida.
  printf "${YELLOW}Creando dependencia:${NC} $HTML\n"
  mkdir -p ../../../html
  cd ../../../html
  git clone -b html --single-branch https://gitlab.com/catalejo_team/catalejo-dev/catalejo-editor-mobile.git
  printf " ${GREEN}Dependencia creada:${NC} $HTML\n"
else
  printf "${BLUE}Ya existe la dependencia:${NC} $HTML\n"
fi


