package catalejoEditor;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import catalejoEditor.activities.MainActivity;

/**
 * This fragments contains and manages the web view that hosts Blockly.
 */
public class BlocklyWebViewFragment extends Fragment {
  protected @Nullable WebView mWebView = null;
  private static final String LOG_TAG = MainActivity.class.getSimpleName();

  @SuppressLint("SetJavaScriptEnabled")
  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    mWebView = new WebView(inflater.getContext());
    mWebView.setWebChromeClient(new WebChromeClient());
    // Enable Javascript in the WebView
    WebSettings webSettings = mWebView.getSettings();
    webSettings.setJavaScriptEnabled(true);
    // Enable interface for methods java in javascript
    mWebView.addJavascriptInterface(new WebAppInterface((MainActivity)getContext()), "Android");
    // load some page in the WebView
    mWebView.loadUrl("file:///android_asset/blockly/webview.html");
    // Cambian el color del webview background
    mWebView.setBackgroundColor(Color.BLACK);
    return mWebView;
  }

  //((MainActivity)getContext()
  /*
  public void start(){
    ((MainActivity)getActivity()).startConnectActivity("");
    Intent i = new Intent(getContext(), ConnectActivity.class);
  }*/


  // TODO: Method to invoke code generation
  // TODO: Method to load workspace from string (or InputStream?)
  // TODO: Method to serialize workspace to string (or OutputStream?)
  // TODO: Clear / reset workspace
  // TODO: Load toolbox
  // TODO: Listener for event JSON
  // TODO: Method to evaluate JavaScript string in the WebView
}
