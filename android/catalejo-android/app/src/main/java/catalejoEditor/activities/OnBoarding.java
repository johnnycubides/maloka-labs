package catalejoEditor.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.blocklywebview.R;

import android.view.Gravity;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;
import android.widget.ImageView;

public class OnBoarding extends AppCompatActivity {

  /** BUTTONS **/
  Button skipButton; // Botón para salir de esta actividad e ir a la actividad home

  //public static Context mContext;
  private static final String LOG_TAG = OnBoarding.class.getSimpleName();

  CarouselView carouselView;
  private int[] gallery = {R.drawable.chica_robot_onboarding, R.drawable.chica_cel_onboarding};
  String[] descriptionText = {"Con esta herramienta podrás crear tus propios artefactos...", "Podrás hackear tus juguetes..."};

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_on_boarding);

    carouselView = findViewById(R.id.carouselView);
    carouselView.setPageCount(gallery.length);
    carouselView.setSlideInterval(4000);
    // carouselView.setImageListener(imageListener);
    carouselView.setViewListener(viewListener);

    /** LISTENER BUTTONS **/
    addListenerSkipButton();
  }

  /** LISTENER SKIP_BUTTON **/
  public void addListenerSkipButton() {
    skipButton = findViewById(R.id.skipButton);
    skipButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startHomeActivity();
      }
    });
  }
  public void startHomeActivity() {
    Intent intent = new Intent(this, HomeActivity.class);
    startActivity(intent);
    finish();
  }

  ViewListener viewListener = new ViewListener() {
    @Override
    public View setViewForPosition(int position) {
      View customView = getLayoutInflater().inflate(R.layout.onboarding_view_custom, null);

      TextView onBoardingLabelTextView = customView.findViewById(R.id.onBoardingLabelTextView);
      ImageView onBoardingImageView = customView.findViewById(R.id.onBoardingImageView);

      onBoardingImageView.setImageResource(gallery[position]);
      onBoardingLabelTextView.setText(descriptionText[position]);
      carouselView.setIndicatorGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);

      return customView;
    }
  };
}
