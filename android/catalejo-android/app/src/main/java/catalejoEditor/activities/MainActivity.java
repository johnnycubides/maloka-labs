package catalejoEditor.activities;


import android.content.Intent;
import android.os.Build;
import android.net.Uri;
import android.content.UriPermission;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import android.content.DialogInterface;
import android.app.AlertDialog;

import com.example.blocklywebview.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import catalejoEditor.Data;

/**
 * The primary activity of the demo application. The activity embeds the
 * {@link catalejoEditor.BlocklyWebViewFragment}.
 */
public class MainActivity extends AppCompatActivity {

  //public static Context mContext;
  private static final String LOG_TAG = MainActivity.class.getSimpleName();
  private static final int REQUEST_CONNECT_ACTIVITY = 1;
  private static final int REQUEST_PROJECTS_ACTIVITY = 2;
  private static final int REQUEST_DOC_ACTIVITY = 3;
  // public static final String EXTRA_MESSAGE = "catalejoEditor.activities.extra.MESSAGE";

  private Data data;
  private Uri projectUri;
  private boolean changesDetectedInBlockly = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    data = new Data();
    Intent intent = getIntent();
    projectUri = intent.getData();
    data.setDataFromActivity(intent.getExtras());  // Toma los valores traidos de la Actividad Main y los asigna a la variable data

    Log.d(LOG_TAG, "catalejo-msg: Created Main Activity");
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
    Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad");
    if (resultCode == RESULT_OK){
      Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad RESULT_OK!");
      if (requestCode == REQUEST_CONNECT_ACTIVITY){
        Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad connect");
        data.setDataFromActivity(resultData.getExtras());
      } else if (requestCode == REQUEST_PROJECTS_ACTIVITY){
        Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad projects");
        data.setDataFromActivity(resultData.getExtras());
        projectUri = resultData.getData();
        this.changesDetectedInBlockly = false; // los cambios en blockly ya fueron considerados
        if (data.getMessageBetweenActivities() == Data.MessageBetweenActivities.RESTART_BLOCKLY){
            returnHome();
        }
      } else if (requestCode == REQUEST_DOC_ACTIVITY){
        Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad docs");
        data.setDataFromActivity(resultData.getExtras());
      }
    } else if (resultCode == RESULT_CANCELED){
      Log.d(LOG_TAG, "catalejo-msg: retorno de la actividad RESULT_CANCELED!");
    }
  }

  //******************************************************************************
  // ** START ** GUARDAR ARCHIVOS EN EL SISTEMA DE FICHEROS DE ANDROID ** START **
  //******************************************************************************
  private void saveJSONFileFromURI(Uri uri, String dataToFile) {
    try {
      ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(uri, "w");
      FileOutputStream fileOutputStream =
        new FileOutputStream(pfd.getFileDescriptor());
      fileOutputStream.write((dataToFile).getBytes());
      // Let the document provider know you're done by closing the stream.
      fileOutputStream.close();
      pfd.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /** Crear el JSONString desde los data **/
  private String JSON2StringData(Data data){
    JSONObject obj = new JSONObject();
    try {
      obj.put("CODE", data.getCode());
      try{
        obj.put("BLOCKLY", data.getBlocklyCode());
      }catch (JSONException e){
        Log.d(LOG_TAG, "No se agrega BLOCKLY a obj JSON");
      }
    }catch (JSONException e){
      Log.d(LOG_TAG, "No se agrega CODE a obj JSON");
    }
    return obj.toString();
  }

  /**
   * Guardar projectos en el sistema de ficheros del android
   * se requiere tener la URI
   */
  public void saveProject(String code, String blocklyCode){
    if (projectUri != null){
      data.setData(code, blocklyCode);
      saveJSONFileFromURI(projectUri, JSON2StringData(data));
      Toast.makeText(getApplicationContext(), "Guardando proyecto", Toast.LENGTH_SHORT).show();
      Log.d(LOG_TAG, "catalejo-msg: guardado automatico");
      this.changesDetectedInBlockly = false; // al guardar cambios
    }else {
      // Toast.makeText(getApplicationContext(), "No se pudo guardar cambios", Toast.LENGTH_SHORT).show();
      Log.d(LOG_TAG, "catalejo-msg: abrir actividad de proyectos");
      this.data.setMessageBetweenActivities(Data.MessageBetweenActivities.FROM_SAVE_BLOCKLY);
      startProjectsActivity(code, blocklyCode);
    }
  }
  //**************************************************************************
  // ** END ** GUARDAR ARCHIVOS EN EL SISTEMA DE FICHEROS DE ANDROID ** END **
  //**************************************************************************

  /**
   * Cambiar data.blocklyCode
   **/
  public void setCodeBlockly(String blocklyCode) {
    data.setBlocklyCode(blocklyCode);
    Toast.makeText(getApplicationContext(), "Guardando snap", Toast.LENGTH_SHORT).show();
  }

  /**
   * Acceso a data.blocklyCode()
   **/
  public String getCodeBlocklyFromData() {
    Toast.makeText(getApplicationContext(), "Cargando snap", Toast.LENGTH_SHORT).show();
    return data.getBlocklyCode();
  }

  /**
   * Acceso a data.code()
   **/
  public String getCodeFromData() {
    return data.getCode();
  }

  public void startConnectActivity(String code, String blocklyCode) {
    this.data.setData(code, blocklyCode);
    Log.d(LOG_TAG, "catalejo-msg: creacion actividad connect");
    Intent intent = new Intent(this.getApplicationContext(), ConnectActivity.class);
    intent.putExtras(this.data.getExtraBundleFromData());
    this.startActivityForResult(intent, REQUEST_CONNECT_ACTIVITY);
    //context.getApplicationContext().startActivity(i);
  }

  public void startProjectsActivityFromButtonProjectWebView(String code, String blocklyCode) {
    this.data.setMessageBetweenActivities(Data.MessageBetweenActivities.FROM_PROJECT_BLOCKLY);
    startProjectsActivity(code, blocklyCode);
    Log.d(LOG_TAG, "catalejo-msg: prueba de actividad proyecto despues de lanzarla");
  }

  public void startProjectsActivity(String code, String blocklyCode) {
    this.data.setData(code, blocklyCode);
    Log.d(LOG_TAG, "catalejo-msg: creacion actividad projects");
    Intent intent = new Intent(this.getApplicationContext(), ProjectsActivity.class);
    intent.setData(this.projectUri);
    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    intent.putExtras(this.data.getExtraBundleFromData());
    this.startActivityForResult(intent, REQUEST_PROJECTS_ACTIVITY);
    //context.getApplicationContext().startActivity(i);
  }

  public void startDocumentationActivity(String code, String blocklyCode) {
    this.data.setData(code, blocklyCode);
    Log.d(LOG_TAG, "catalejo-msg: creacion actividad docs");
    Intent intent = new Intent(this.getApplicationContext(), DocumentationActivity.class);
    intent.putExtras(this.data.getExtraBundleFromData());
    this.startActivityForResult(intent, REQUEST_DOC_ACTIVITY);
  }

  public void reportChangesInBlockly() {
    this.changesDetectedInBlockly = true;
    // Log.d(LOG_TAG, "catalejo-msg: changesDetectedInBlockly "+this.changesDetectedInBlockly);
  }

  public void returnToHome() {
    if (!this.changesDetectedInBlockly) {
      returnHome();
    } else {
      DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          switch (which){
            case DialogInterface.BUTTON_POSITIVE:
            //Yes button clicked
            returnHome();
            break;
            case DialogInterface.BUTTON_NEGATIVE:
            //No button clicked
            break;
          }
        }
      };

      AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
      builder.setMessage("No has guardado tus cambios.\n\n ¿Deseas salir sin guardar?").setPositiveButton("Sí", dialogClickListener)
        .setNegativeButton("No", dialogClickListener).show();
    }
  }

  @Override
  public void onBackPressed() {
    returnToHome();
  }

  public void returnHome(){
    Intent returnToHome = new Intent(); // Asigna memoria para retornar datos a la Actividad Home
    returnToHome.setData(projectUri);
    returnToHome.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
    returnToHome.putExtras(data.getExtraBundleFromData());
    setResult(RESULT_OK, returnToHome); // Se entrega el resultado OK con el Intent
    finish(); // Se detienen la actividad Main
    Log.d(LOG_TAG, "catalejo-msg: finalizando la actividad Main");
  }

}

