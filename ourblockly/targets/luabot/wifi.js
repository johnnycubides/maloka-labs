// BEGIN WIFI MODE STATION
Blockly.Blocks['wifi_sta'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/wifi_sta.gif", 30, 30, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("WiFi en modo estación");
    this.appendValueInput("redName")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Nombre Red");
    this.appendValueInput("password")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Contraseña");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['wifi_sta'] = function(block) {
  var value_redname = Blockly.Lua.valueToCode(block, 'redName', Blockly.Lua.ORDER_ATOMIC);
  var value_password = Blockly.Lua.valueToCode(block, 'password', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'gpio.mode(4, gpio.OUTPUT)\n'+
		'gpio.write(4,gpio.HIGH)\n'+
		'wifi_alarm = tmr.create()\n'+
		'wifi_alarm:register( 1000, tmr.ALARM_AUTO, function()\n'+
		'	if wifi.sta.status() == wifi.STA_CONNECTING then\n'+
		'		gpio.write(4,gpio.LOW)\n'+
		'		wifi_alarm:stop()\n'+
		'	end\n'+
		'end)\n'+
		'station_cfg={}\n'+
		'station_cfg.ssid='+value_redname+'\n'+
		'station_cfg.pwd='+value_password+'\n'+
		'station_cfg.save=true\n'+
		'wifi.sta.config(station_cfg)\n'+
		'wifi.setmode(wifi.STATION)\n'+
		'wifi_alarm:start()\n';
  return code;
};
// END

// BEGIN WIFI MODE ACCES POINT
Blockly.Blocks['wifi_ap'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/wifi_ap.gif", 30, 30, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("WiFi en modo punto de acceso");
    this.appendValueInput("redName")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Nombre Red");
    this.appendValueInput("password")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Contraseña");
	this.appendValueInput("channel")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Canal");
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Lua['wifi_ap'] = function(block) {
  var value_redname = Blockly.Lua.valueToCode(block, 'redName', Blockly.Lua.ORDER_ATOMIC);
  var value_password = Blockly.Lua.valueToCode(block, 'password', Blockly.Lua.ORDER_ATOMIC);
  var value_channel = Blockly.Lua.valueToCode(block, 'channel', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'cfg={}\n'+
		'cfg.ssid='+value_redname+'\n'+
		'cfg.pwd='+value_password+'\n'+
		'cfg.channel='+value_channel+'\n'+
		'wifi.ap.config(cfg)\n'+
		'wifi.setmode(wifi.SOFTAP)\n';
  return code;
};
//END
// BEGIN WIFI MODE ACCES POINT AND MODE STATION
Blockly.Blocks['wifi_ap_sta'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("WiFi en ambos modos");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Como Punto de Acceso");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/wifi_ap.gif", 30, 30, "*"));
    this.appendValueInput("redNameAP")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Nombre Red");
    this.appendValueInput("passwordAP")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Contraseña");
	this.appendValueInput("channel")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Canal");
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("dependencies/modulesNodeMCU/javascript/imag/wifi_sta.gif", 30, 30, "*"));
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Como Estación");
    this.appendValueInput("redNameSTA")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Nombre Red");
    this.appendValueInput("passwordSTA")
        .setCheck("String")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("Contraseña");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(265);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};
Blockly.Lua['wifi_ap_sta'] = function(block) {
  var value_rednameap = Blockly.Lua.valueToCode(block, 'redNameAP', Blockly.Lua.ORDER_ATOMIC);
  var value_passwordap = Blockly.Lua.valueToCode(block, 'passwordAP', Blockly.Lua.ORDER_ATOMIC);
  var value_channel = Blockly.Lua.valueToCode(block, 'channel', Blockly.Lua.ORDER_ATOMIC);
  var value_rednamesta = Blockly.Lua.valueToCode(block, 'redNameSTA', Blockly.Lua.ORDER_ATOMIC);
  var value_passwordsta = Blockly.Lua.valueToCode(block, 'passwordSTA', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = 'station_cfg={}\n'+
		'station_cfg.ssid='+value_rednamesta+'\n'+
		'station_cfg.pwd='+value_passwordsta+'\n'+
		'cfg.channel='+value_channel+'\n'+
		'station_cfg.save=true\n'+
		'wifi.sta.config(station_cfg)\n'+
		'cfg={}\n'+
		'cfg.ssid='+value_rednameap+'\n'+
		'cfg.pwd='+value_passwordap+'\n'+
		'wifi.ap.config(cfg)\n'+
		'wifi.setmode(wifi.STATIONAP)\n';
  return code;
};
//END
