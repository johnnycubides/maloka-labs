# Produción de la aplicación

[Fuente](https://codiwans.com/2017/05/18/11-pasos-a-tener-en-cuenta-durante-la-produccion-de-una-app/)

[creación de aplicaciones moviles unal](https://sites.google.com/site/movilesunal20202/)

## A. Fase de PRE - Producción

### 1. Definición del producto

* Modelo canvas

### 2. Wireframes mockup

* La experiencia de usuario
* La estructura de navegación (flow)
* Las funcionalidades de cada pantalla

## B. Fase de desarrollo

### 3. Diseño

* Aspecto visual según Wireframes

### 4. Programar el Front-end

### 5. Diseñar la estructura de la base de datos

### 6. Arquitectura y lógica de Servidor (backend)

### 7. Creación de una API

### 8. Test

User tests, detección de errores, tests unitarios, tests de integración, multiplataforma, dispositivos móviles, SO móvil

### 9. Desplegar en preproducción

## C. Fase de publicación

### 10. Publicación en store

## D. Fase de mantenimiento

### 11. Mantenimiento y mejoras


