# Luabot (basado en NodeMCU + Catalejo editor)


![ejemplos de proyectos](./img/index/presentation1.png)

Para hacer uso de ésta documentación debes contar con:

* Aplicación *Catalejo Editor* para *Android*
* Contar con la tarjeta:
    * [Tarjeta nodemcu](nodemcu_v1)

## Comunidad

Ésta documentación está en continua construcción te invitamos a participar compartiendo tu
experiencia desarrollando tus proyectos. 

Te invitamos a ser parte de nuestro servidor en discord, dale clic en la imagen:

<a href="https://discord.gg/4GxYxyy">
<img src="./img/index/discord-1.gif" alt="./img/index/discord-1.gif" style="width:20%;">
</a>

[Link de invitación](https://discord.gg/4GxYxyy)

O también en el siguiente foro público, dale clic en la imagen:

<a href="https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu">
<img src="./img/index/flarum.png" alt="./img/index/flarum.png" style="width:20%;">
</a>

[Link de invitación:](https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu)

¡Estaremos felices de leerte!

## Agradecimientos

Agradecemos la colaboración de cada estudiante que ha aportado su grano de arena
para ayudarnos a generar una documentación a ésta valiosa herramienta que hemos denominado
LuaBot.

Atentamente:

<img src="https://s.gravatar.com/avatar/9d9438c3f89fc9ec7c2e26f31f3090e4?s=80">
<p>
Johnny Cubides<br>
e-mail: jgcubidesc@unal.edu.co<br>
discord: johnnycubides#0705<br>
telegram: @johnny5<br>
<p>
