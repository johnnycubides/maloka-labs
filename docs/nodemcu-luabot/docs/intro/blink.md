# Hola Mundo

El hola mundo es el primer acercamiento que tienes con la tarjeta
de desarrollo y con el software de programación. A continuación
encontrarás imágenes y un vídeo que te puede orientar sobre cómo
encender tu primer LED. ¡Disfruta del contenido!.

## Materiales

Para desarrollar éste ejemplo necesitarás:

* [Tarjeta NodeMCU](../../nodemcu_v1) con el [software instalado](../../install/firmware)
* [Software Catalejo Editor](../../install/android)
* Resistencia de 220 Omhs o 330 Ohms o 470 Ohms
* Un LED de cualquier color
* Un conector rápido (Jumper) macho-macho
* Un tablero de prototipos (Protoboard)
* Un cable USB para energizar la tarjeta
* [Cargador o baterías](../../intro/energize-card) (PowerBank)
* Muchas ganas de aprender y paciencia

## Creando nuestro circuito

Lo primero que debes hacer es imitar de la siguiente imagen la
manera y los lugares donde se conecta cada elemento del circuito
a armar, comprobando las veces que sea necesario nuestro montaje
con el diagrama pictografico, ¡Manos a la obra!

![Ejemplos de conexión](../img/intro/blink/nodemcu_led_bb.png)

## Construyamos nuestro programa

Abre la aplicación *Catalejo Editor* juega un poco con ella
buscando de programación los bloques y cuando la hayas explorado
lo suficiente trata de armar el siguiente código de bloques,
revisa muchas veces que te quede igual.

![Ejemplo de programa](../img/intro/blink/nodemcu-blink.png)

Desde luego hay detalles que se te escaparán y muchas dudas
tendrás; ten presente que es difícil representar todas las
respuestas en ese par de imágenes, para ello te invitamos
a ver el siguiente vídeo que despejará unas dudas y desde
luego generará otras. No olvides que puedes usar nuestro
servidor de 
[Discord](https://discord.gg/4GxYxyy)
o
[nuestro foro](https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu)
para hacer esas preguntas que te quedan, estaremos prestos a apoyarte.

## Vídeo que te responderá dudas


<iframe width="560" height="315" src="https://www.youtube.com/embed/WT7xEaLsfng" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

