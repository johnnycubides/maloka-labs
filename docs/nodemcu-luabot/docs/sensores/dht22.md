# DHT22

Sensor de humedad y temperatura (relativa)

## Ejemplo de conexión

![dht22](../img/sensores/dht22/dht22_bb.png)

## Ejemplo de algoritmo

![dhtt](../img/sensores/dht22/dht22_luabot.png)

