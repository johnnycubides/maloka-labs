# Ultrasonido

## Ejemplos

### Conexiones

![conexiones](../img/sensores/ultrasonido/nodemcu_ultrasonido_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Algoritmo

![algoritmo](../img/sensores/ultrasonido/ultrasonido.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

