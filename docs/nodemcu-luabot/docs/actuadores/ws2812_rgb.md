# LEDs RGB one wire

Estos LEDs usan un protocolo denominado ws2812

## Ejemplo de conexión

Tener en cuenta, éste tipo de LEDs solo pueden ser conectados en el pin **D4** en la *nodemcu*

![esquema de conexion](../img/actuadores/ws2812/nodemcu_rgb_ws2812_16_leds_bb.png)

## Ejemplo de algoritmo

El siguiente algitmo enciende los 16 LEDs del anillo de manera secuencias cambiando los colores
pasando de rojo a verde y de verde a azul.

Parte uno: encender los LEDs de manera secuencias hasta que todos queden de color rojo.

![leds_rgb_ow_1](../img/actuadores/ws2812/ws2812_1.jpg)

Parte dos: todos los LEDs pasaran de rojo a verde

![leds_rgb_ow_2](../img/actuadores/ws2812/ws2812_2.jpg)

Parte tres: finalmente todos los LEDs pasarán del color verda al color azul y finaliza.

![leds_rgb_ow_3](../img/actuadores/ws2812/ws2812_3.jpg)

